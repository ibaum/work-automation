class GDK
  GEO_DIR = '/Users/ibaum/gdk/geo'.freeze
  REPO = 'git@gitlab.com:gitlab-org/gitlab-development-kit.git'.freeze

  class << self
    def init_command(node)
      "git clone #{GDK::REPO} #{node}"
    end

    def primary_dir
      File.join(GDK::GEO_DIR, 'primary')
    end

    def secondary_dir
      File.join(GDK::GEO_DIR, 'secondary')
    end

    def stop(site)
      dest_dir = send("#{site}_dir")
      return unless Dir.exist?(send(dest_dir))

      system("cd #{dest_dir} && #{rvm_do} gdk stop")
    end
  end
end
