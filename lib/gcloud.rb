require 'google/cloud/compute'

module Gcloud
  class Instances
    PROJECT = 'group-geo-f9c951'.freeze

    class << self
      def stop_all
        do_on_all(:stop)
      end

      def start_all
        do_on_all(:start)
      end

      def list_all
        puts 'NI: list_all'
      end

      def do_on_all(cmd)
        ENV['COMPUTE_CREDENTIALS'] = File.join(ENV.fetch('PWD'), './get/keys/ibaum-automation.json')
        client = Google::Cloud::Compute.instances
        client.aggregated_list(filter: 'name:ibaum-*', project: PROJECT).each do |result, operation|
          zone = result.split('/').last
          operation.instances.each do |instance|
            puts "#{cmd}ing #{instance.name} in #{zone}"
            client.send(cmd, instance: instance.name, project: PROJECT, zone: zone)
          end
        end
      end
    end
  end
end
