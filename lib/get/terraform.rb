require_relative '../get'

class GET
  class Terraform
    PROJECT = 'https://gitlab.com/api/v4/projects/7895893'.freeze
    VERSION = '1.0.11'.freeze
    NO_VARS = %w[validate providers].freeze
    CONFIG_BASE = "get/inventories/terraform".freeze

    class << self
      def init(inst, config_path, upgrade: false)
        dir = inst.eql?('shared') ? "#{CONFIG_BASE}/../../terraform/#{inst}" : "#{CONFIG_BASE}/#{inst}"
        "cd #{dir} && (terraform show > /dev/null || terraform init -backend-config=#{config_path} -reconfigure #{'-upgrade' if upgrade})"
      end

      def cmd(cmd, inst, extra: '')
        size = ENV.key?('SIZE') ? ENV['SIZE'] : '1k'
        terraform_dir = if Dir.exist?(File.join(CONFIG_BASE, size, inst))
                          File.join(CONFIG_BASE, size, inst)
                        else
                          File.join(CONFIG_BASE, inst)
                        end
        extra << "#{extra} #{add_ips}" if inst.eql?('shared') && !NO_VARS.include?(cmd)
        # extra << %( -var-file="../../../../#{CONFIG_BASE}/#{inst}/vars.tfvars.json") unless inst.eql?('shared')
        %(cd #{terraform_dir} && terraform #{cmd} #{extra})
      end

      def apply(inst)
        return cmd('apply', inst, extra: '-auto-approve') unless inst.eql?('shared')

        extra = '-auto-approve'
        extra << add_ips
        cmd('apply', inst, extra: extra)
      end

      def add_ips
        %w[primary secondary].map do |foo|
          ei = external_ip(foo)
          next if ei.nil?

          " -var #{foo}_ip=#{ei}"
        end.join(' ')
      end

      def extra_cmd(inst)
        path = "#{PROJECT}/terraform/state/get-#{inst}-#{GET.size}"
        <<~EOEXTRA_CMD.chomp
          address = "#{path}"
          lock_address = "#{path}/lock"
          unlock_address = "#{path}/lock"
          username = "ibaum"
          password = "#{GET.personal_access_token}"
          lock_method = "POST"
          unlock_method = "DELETE"
          retry_wait_min = "5"
        EOEXTRA_CMD
      end

      def show(inst)
        cmd('show', inst)
      end

      private

      def external_ip(instance)
        data = JSON.parse(`cd #{CONFIG_BASE}/#{instance} && terraform show -json`)
        data['values']['outputs']['external_ip_addr']['value'] if data.key?('values')
      end
    end
  end
end
