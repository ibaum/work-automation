namespace :laptop do
  def config
    YAML.safe_load(File.read('configs/laptop.yml'))
  end

  desc 'Fetch some importatn repos'
  task :repos do
    config['repos'].each do |repo|
      sh "cd #{repo} && git fetch"
    end
  end
end
