namespace :qa do
  desc 'Run against an instance: Accepts GITLAB_URL USERNAME PASSWORD DIRECTORY TAG TYPE IMAGE '
  task run_specs: ['rvm:setup:qa'] do
    url = ENV.fetch('GITLAB_URL', URL)
    username = ENV.fetch('USERNAME', 'root')
    password = ENV.fetch('PASSWORD', root_password)
    directory = ENV.fetch('DIRECTORY', "#{ENV.fetch('PWD')}/qa_results")
    tag = ENV.fetch('TAG', 'master')
    type = ENV.fetch('TYPE', 'ee')
    image = ENV.fetch('IMAGE', "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-#{type}-qa:#{tag}")
    instance = ENV.fetch('INSTANCE', 'Test::Instance::Any')
    sh <<~EOFCOMMAND
      QA_ARTIFACTS_DIR=#{directory}  \
      GITLAB_USERNAME=#{username} \
      GITLAB_PASSWORD=#{password} \
      rvm #{RVM_RUBY_VERSION}@#{QA_RVM} do gitlab-qa #{instance} #{image} #{url}
    EOFCOMMAND
  end

  task :clean do
    sh 'find qa_results -type d -mtime +30 -depth 1 -delete'
  end
end
