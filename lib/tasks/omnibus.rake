license_file = File.join(Dir.pwd, 'configs', 'license.yml')
omnibus_dir = '/Users/ibaum/src/omnibus-gitlab/master'

VOLUME_BASE = '/Users/ibaum/docker_volumes/gitlab'.freeze

def rvm
  ENV.key?('RVM') ? ENV['RVM'] : '2.7.2@on'
end

namespace :omnibus do
  namespace :env do
    namespace :bundle do
      desc 'Install the bundled requirements'
      task :install do
        sh "cd #{omnibus_dir} && rvm #{rvm} do bundle install"
      end
    end
  end

  namespace :specs do
    desc 'Run specs'
    task runqa: ['omnibus:env:bundle:install'] do
      # branch = ENV['BRANCH']
      spec_file = ENV.fetch('SPEC_FILE')
      report_file = "#{Dir.home}/tmp/knapsack_dummy_report.json"
      failure_file = "#{Dir.home}/tmp/failures.txt"
      knapsack_env = "KNAPSACK_REPORT_PATH=#{report_file} KNAPSACK_GENERATE_REPORT=true USE_KNAPSACK=true"

      rspec_options = '--color --format RspecJunitFormatter --out junit_rspec.xml --format documentation'
      rspec_options << ' --only-failures' if File.exist?(failure_file)
      rspec_options << " #{spec_file}" unless spec_file.nil?
      sh "cd #{omnibus_dir} && #{knapsack_env} rvm #{rvm} do bundle exec rake \"knapsack:rspec[#{rspec_options}]\""
    end
  end

  namespace :patroni do
    ansible_branch = 'do-patroni'

    desc 'Setup a patroni clustered omnibus install: JOB -- ID of the job to download the package from.'
    task :install do
      sh "#{build_ansible(ansible_inventory('patroni'), license_file, ansible_branch)} newha.yml"
    end
  end

  namespace :docker do
    desc 'Start the docker image'
    task :up do
      version = ENV.fetch('VERSION', 'latest')
      image = ENV.fetch('IMAGE', 'gitlab/gitlab-ee')

      full_image = ENV.fetch('FULL_IMAGE', "#{image}:#{version}")
      command = 'docker run --detach --publish 8443:443 --publish 8080:80 --publish 8022:22 --name gitlab '
      command << "--volume #{VOLUME_BASE}/config:/etc/gitlab " \
                 "--volume #{VOLUME_BASE}/logs:/var/log/gitlab " \
                 "--volume #{VOLUME_BASE}/data:/var/opt/gitlab"
      sh "#{command} #{full_image}"
    end

    desc 'Stop the docker image'
    task :down do
      sh 'docker stop gitlab || true'
    end

    desc 'Remove the docker image'
    task remove: %i[down] do
      sh 'docker rm gitlab || true'
    end

    desc 'Redploy from scratch'
    task redeploy: %i[remove up]

    desc 'Clean all of the persistent data'
    task clean: %i[remove] do
      sh "rm -rf #{VOLUME_BASE}"
    end
  end
end
