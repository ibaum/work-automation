def set_values
  version = ENV.fetch('BRANCH', 'master')
  "gitlab.unicorn.image.tag=#{version},gitlab.unicorn.workhorse.tag=#{version}"
end

def url
  config = YAML.safe_load(File.read(VALUES_FILE))
  "https://gitlab.#{config['global']['hosts']['domain']}"
end

def values_file
  ENV.key?('VALUES_FILE') ? ENV['VALUES_FILE'] : VALUES_FILE
end

namespace :helm do
  desc 'Display helm and tiller version'
  task :version do
    sh "#{helm_command} version"
  end

  desc 'Show what is installed'
  task :list do
    sh "#{helm_command} list"
  end

  desc 'Lint the chart, no errors please'
  task :lint do
    unless ENV.key?('NO_LINT')
      sh "#{helm_command} lint -f #{values_file}" # --set #{set_values}"
    end
  end

  desc 'Delete and rebuild everything'
  task rebuild: ['helm:delete:all', 'helm:upgrade:repo']

  desc 'Run qa against the deployed instance'
  task :qa do
    ENV['GITLAB_URL'] = url
    Rake.application.invoke_task('qa:run')
  end

  desc 'Run the specs against the deployed instance'
  task :specs do
    p, u = url.split('://')
    ENV['PROTOCOL'] = p
    ENV['GITLAB_URL'] = u
    ENV['S3_ENDPOINT'] = 'https://minio.admiraloreally.com'
    secret_name = "#{RELEASE_NAME}-minio-secret"
    ENV['S3_ACCESS_KEY'] = get_secret(secret_name, '.data.accesskey')
    ENV['S3_SECRET_KEY'] = get_secret(secret_name, '.data.secretkey')
    ENV['RELEASE_NAME'] = RELEASE_NAME
    ENV['GITLAB_PASSWORD'] = root_password
    ENV['GITLAB_ROOT_DOMAIN'] = 'admiraloreally.com'
    file = if ENV.key?('FILE')
             ENV['FILE']
           else
             'spec/features'
           end
    cmd =  "cd #{CHART_DIR} && rvm 2.6.6@charts do bundle exec rspec #{file}"
    sh cmd
  end

  desc 'Install the CE version'
  task ce: [:lint, 'kubernetes:cluster:create', 'helm:dependency:update'] do
    ENV['CLUSTER_NAME'] = CLUSTER
    ENV['PROJECT'] = PROJECT
    command = "#{helm_command} upgrade"
    command << ' --dry-run' if ENV['dry_run'].eql?('true')
    sh "#{command} --install gitlab . -f #{values_file} --set #{set_values},global.edition=ce"
  end

  desc 'Redeploy from scratch'
  task redeploy: ['tiller:upgrade', :upgrade]

  desc 'Open the instance in a web page'
  task open: ['kubernetes:root'] do
    sh "open #{url}"
  end

  desc 'Print the templates'
  task print: [:lint] do
    ENV['debug'] = 'true'
    sh "#{helm_command} install --dry-run . -f #{values_file}"
  end

  desc 'Render the templates -- TEMPLATE_FILE only reference the named template file'
  task :template do
    unless ENV.key?('NO_TEMPLATE')
      command = "#{helm_command} template . -f #{values_file}"
      command << " -x #{ENV['TEMPLATE_FILE']}" if ENV.key?('TEMPLATE_FILE')
      sh command
    end
  end

  namespace :application do
    namespace :migrations do
      desc 'Wait for the migrations job to complete'
      task :wait do
        while `kubectl get jobs --field-selector=status.successful=1 -l app=migrations -o 'jsonpath={.items[*].metadata.name}'`.empty?
          puts 'Waiting for migrations to complete'
          sleep 5
        end
      end
    end

    namespace :task_runner do
      desc 'Wait until the task runner is available'
      task :wait do
        10.times do
          break unless pod_name.nil?

          puts 'task-runner not here yet'
        end
      end
    end

    namespace :signups do
      desc 'Disable signups for the instance'
      task disable: ['helm:application:migrations:wait'] do
        sh <<~COMMAND
          kubectl exec -it #{task_runner_pod} gitlab-rails runner "g = Gitlab::CurrentSettings.current_application_settings ; g.signup_enabled = false ; g.save!"
        COMMAND
      end
    end
  end

  namespace :config do
    desc 'Print the current config'
    task :print do
      pp YAML.safe_load(File.read('mykube.yaml'))
    end

    desc 'Add a config value'
    task :add do
      data = YAML.safe_load(File.read(VALUES_FILE))
      data['foo'] = 'yomama'
      pp data.to_yaml
    end

    desc 'Remove a config value'
    task :delete
  end

  namespace :delete do
    desc 'Get rid of the installed chart'
    task :chart do
      sh "#{helm_command} delete ibaum-gitlab || true"
      %w[pods jobs pvc].each do |object|
        sh "kubectl -n foo delete #{object} -l release=ibaum-gitlab"
      end
      sh 'kubectl delete secret ibaum-gitlab-gitlab-runner-secret || true'
    end

    desc 'Delete everything'
    task all: [:chart, 'kubernetes:delete:namespace']
  end

  namespace :dependency do
    desc 'update dependencies'
    task :update do
      sh "#{helm_command} dependency update"
    end
  end

  namespace :repo do
    desc 'update the repo'
    task :update do
      sh 'helm repo update'
    end

    desc 'add the gitlab repo'
    task :add do
      sh 'helm repo add gitlab https://charts.gitlab.io'
    end
  end

  namespace :tiller do
    desc 'Upgrade the installed tiller'
    task :upgrade do
      # sh "kubectl create namespace foo && #{helm_command} init --upgrade --tiller-namespace foo"
      sh "#{helm_command} init --upgrade --tiller-namespace foo --service-account tiller"
      sh 'kubectl -n foo create serviceaccount tiller'
    end
  end

  namespace :upgrade do
    desc 'Upgrade the chart from my local checkout'
    task repo: [:lint, 'kubernetes:context', 'kubernetes:cluster:create'] do
      ENV['CLUSTER_NAME'] = CLUSTER
      ENV['PROJECT'] = PROJECT
      command = "#{helm_command} upgrade"
      command << ' --dry-run' if ENV['dry_run'].eql?('true')
      sh "#{command} --namespace foo --install ibaum-gitlab . -f #{values_file}"
      Rake.application.invoke_task('helm:application:signups:disable')
    end.enhance

    desc 'Upgrade the chart from a released version using the operator'
    task release: ['helm:repo:update', 'kubernetes:cluster:create'] do
      ENV['CLUSTER_NAME'] = CLUSTER
      ENV['PROJECT'] = PROJECT
      command = 'helm upgrade'
      command << ' --dry-run' if ENV['dry_run'].eql?('true')
      command << " --version #{ENV['VERSION']}" if ENV.key?('VERSION')
      sh "#{command} --namespace foo --install ibaum-gitlab gitlab/gitlab -f #{values_file}"
    end
  end
end
