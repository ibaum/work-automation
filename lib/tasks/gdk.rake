require 'fileutils'
require_relative '../gdk'

# RVM_RUBY = '2.7.5@gdk'.freeze

namespace :gdk do
  desc 'Do the needful'
  task :prep do
    mkdir_p GDK::GEO_DIR
    sh 'bundle install'
    sh 'gem install gitlab-development-kit'
  end

  desc 'Stop both'
  task stop: %w[gdk:primary:stop gdk:secondary:stop]
end
