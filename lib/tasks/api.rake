endpoint = ENV.fetch('ENDPOINT', 'https://gitlab.com/api/v4')

namespace :api do
  desc 'Get timings for CNG builds'
  task :cng_build_timings do
    token = ENV.fetch('GL_TOKEN')
    raise 'Please provide your token with GL_TOKEN' if token.nil?

    project = CGI.escape('gitlab-org/gitlab-qa-mirror')
    base_url = "https://gitlab.com/api/v4/projects/#{project}"
    pipeline_url = "#{base_url}/pipelines?status=success&per_page=100"
    base_cmd = "curl -g -s --header 'Private-Token: #{token}'"
    data =  JSON.parse(`#{base_cmd} '#{pipeline_url}'`)
    timings = Hash.new { |hash, key| hash[key] = [] }
    count = 0
    data.each do |pipeline|
      jobs = JSON.parse(`#{base_cmd} '#{base_url}/pipelines/#{pipeline['id']}/jobs'`)
      next if Date.parse(jobs.first['started_at']) < Date.parse('July 11 2018')

      count += 1
      jobs.each do |job|
        timings[job['name']] << job
      end
    end
    puts '| Job | Average Time |'
    puts '| --- | ------------ |'
    timings.each_pair do |job, results|
      foo = results.map { |x| Time.parse(x['finished_at']) - Time.parse(x['started_at']) }
      puts "| #{job} | #{foo.inject { |sum, el| sum + el }.to_f / foo.size} |"
    end
  end

  desc 'Get timings for QA pipelines'
  task :qa_build_timings do
    gitlab = Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: gitlab_token)
    project = 'gitlab-org/gitlab-qa-mirror'
    pipelines = gitlab.pipelines(project, status: 'success', updated_after: '2020-12-10', updated_before: '2020-12-19')
    timings = Hash.new { |hash, key| hash[key] = [] }
    statuses = Hash.new { |hash, key| hash[key] = [] }
    count = 0
    pipelines.auto_paginate do |pipeline|
      puts "We're on #{pipeline.created_at}"
      # next unless pipeline.status.eql?('success')
      # next unless pipeline.created_at.match(/2020-12-1/)

      jobs = gitlab.pipeline_jobs(project, pipeline.id, status: 'success')
      jobs.auto_paginate do |job|
        statuses[job.name] << job.status
        next unless job.status.eql?('success')

        timings[job.name] << job
      end
      count += 1
    end

    File.open('jobs.txt', 'w') do |f|
      f.puts 'job,Started At,Run Time'
      timings.each_pair do |job, results|
        results.each do |j|
          f.puts "#{job},#{j.started_at},#{Time.parse(j.finished_at) - Time.parse(j.started_at)}"
        end
      end
    end
    statuses.each_pair do |job, status|
      puts "#{job}: #{status.sort.uniq}"
    end
    puts '| Job | Average Time |'
    puts '| --- | ------------ |'
    timings.each_pair do |job, results|
      foo = results.map { |x| Time.parse(x.finished_at) - Time.parse(x.started_at) }
      puts "| #{job} | #{foo.inject { |sum, el| sum + el }.to_f / foo.size} |"
    end
  end

  namespace :pipeline do
    desc 'Get a pipelines job logs'
    task :logs do
      project = CGI.escape('gitlab/omnibus-gitlab')
      base_url = "https://dev.gitlab.org/api/v4/projects/#{project}"
      pipeline_url = "#{base_url}/pipelines/90542/jobs"
      base_cmd = "curl -g -s --header 'Private-Token: #{dev_gitlab_token}'"
      data = JSON.parse(`#{base_cmd} #{pipeline_url}`)
      data.each do |job|
        first_trace_url = "#{base_url}/jobs/#{job['id']}/trace"
        pp `#{base_cmd} #{first_trace_url}`
      end
    end

    desc 'Retry a pipeline until it passes'
    task :retry do
      project = 'gitlab/omnibus-gitlab'
      gitlab = Gitlab.client(endpoint: endpoint, private_token: dev_gitlab_token)
      pipeline_id = ENV.fetch('PIPELINE')
      raise 'Please provide PIPELINE' if pipeline_id.nil?

      while gitlab.pipelines(project, id: pipeline_id).first.status.eql?('running')
        puts 'Still running'
        sleep 10
      end
    end
  end

  namespace :mirrors do
    desc 'List omnibus-mirror projects'
    task :list do
      raise 'Token please' unless ENV.key?('TOKEN')

      group_name = 'gitlab-org/build/omnibus-mirror'
      gitlab = Gitlab.client(endpoint: endpoint, private_token: ENV.fetch('TOKEN'))
      group = gitlab.group(group_name)
      gitlab.group_projects(group.id).each do |project|
        puts project.public_methods.sort
        break
      end
    end
  end

  desc 'Find user id'
  task :id do
    raise 'Token please' unless ENV.key?('TOKEN')
    raise 'Username please' unless ENV.key?('USERNAME')

    gitlab = Gitlab.client(endpoint: endpoint, private_token: ENV.fetch('TOKEN'))

    user = gitlab.user_search(ENV.fetch('USERNAME'))
    puts user.first.id
  end

  namespace :images do
    namespace :dev do
      desc 'Charts image timings'
      task :timings do
        require 'gitlab'
        require 'time'
        token = ENV.fetch('TOKEN')
        raise 'Token please' unless token

        gl = Gitlab.client(endpoint: 'https://dev.gitlab.org/api/v4', private_token: token)

        cumulative = 0
        count = 0
        options = { scope: 'tags', updated_afer: '2020-09-01' }
        skip = %(failed cancelled skipped)
        gl.pipelines('gitlab/charts/components/images', options).auto_paginate do |job|
          next if skip.include?(job.status)

          started = Time.parse(job.created_at)
          ended = Time.parse(job.updated_at)
          time = (ended - started) / 60
          next if time > 200.0

          cumulative += time
          count += 1
        end

        puts "Average: #{cumulative / count}"
      end
    end
  end
end
