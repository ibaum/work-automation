require_relative '../gcloud.rb'

namespace :gcloud do
  namespace :containers do
    namespace :clusters do
      task :upgrade do
        sh "gcloud container clusters upgrade #{CLUSTER}"
      end
    end
  end

  namespace :instances do
    desc 'stop all nodes'
    task :stop do
      Gcloud::Instances.stop_all
    end

    desc 'start all nodes'
    task :start do
      Gcloud::Instances.start_all
    end

    desc 'list all nodes'
    task :list do
      Gcloud::Instances.list_all
    end
  end
end
