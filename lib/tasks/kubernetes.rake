namespace :kubernetes do
  ENV['CLUSTER_NAME'] = CLUSTER
  ENV['PROJECT'] = PROJECT

  desc 'Set the context to my cluster'
  task :context do
    sh "kubectx gke_#{PROJECT}_us-central1-b_#{CLUSTER}"
  end

  desc 'Show the cluster'
  task :show do
    if Kubernetes.cluster_exist?
      puts 'we have cluster'
    else
      puts 'no cluster'
    end
  end

  desc 'Copy the root password to the clipboard'
  task :root_password do
    system "echo #{root_password} | pbcopy"
  end

  desc 'Copy the grafana password to the clipboard'
  task :grafana do
    system "echo #{grafana_password} | pbcopy"
  end

  desc 'Copy the postgresql password to the clipboard'
  task :postgresql do
    system "echo #{postgresql_password} | pbcopy"
  end

  desc 'Copy the postgresql superuser password to the clipboard'
  task :postgresql_superuser do
    system "echo #{postgresql_superuser_password} | pbcopy"
  end

  task :root_print do
    system "echo #{root_password}"
  end

  namespace :backup do
    desc 'Just the database'
    task :database do
      $stdout.sync = true
      sh 'echo foo'
      sh 'kubectl get pods -l app=task-runner -o custom-columns=N:.metadata.name --no-headers'
      # sh "kubectl exec -it #{pod} -- backup-utility --skip repositories,registry"
    end
  end

  namespace :cluster do
    desc 'Delete the cluster'
    task delete: :context do
      sh "#{CHART_DIR}/scripts/gke_bootstrap_script.sh down" if Kubernetes.cluster_exist?
    end

    desc 'Create the cluster'
    task create: :context do
      sh "#{CHART_DIR}/scripts/gke_bootstrap_script.sh up" unless Kubernetes.cluster_exist?
    end

    desc 'Recreate the cluster'
    task recreate: %i[context delete create]

    desc 'What we got'
    task info: :context do
      sh 'kubectl cluster-info'
    end
  end

  namespace :delete do
    desc 'Get rid of the secrets'
    task :secrets do
      sh 'kubectl delete secret --all'
    end

    desc 'Delete the namespace'
    task :namespace do
      sh 'kubectl delete namespace foo'
    end
  end
end
