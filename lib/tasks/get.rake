# ENV variables to pass
# PAT: Personal Access Token for terraform state
# SSH_KEY_FILE: Public key to auth to servers

def push(letter, dir)
  i = 0
  loop do
    url = "ssh://git@primary.get.ibaum.gogitlab.xyz:2222/bulk/gitlab-shell-#{letter}#{i}.git"

    puts "pushing #{url}"
    `git -C #{File.join(dir, 'gitlab-shell')} push --all #{url}`

    i += 1
  end
end

namespace :get do
  desc 'Make sure things are ready'
  multitask prep: %w[terraform:prep ansible:prep]

  desc 'Open the primary page in a web browser'
  task :open do
    sh 'open https://primary.get.ibaum.gogitlab.xyz'
  end

  desc 'Fetch a local copy of the source'
  task :clone do
    mkdir_p GET::SOURCE_DIR
    sh "cd #{GET::SOURCE_DIR} && git clone #{GET::REPO} -b #{GET::VERSION}"
  end

  desc 'Programtically get a local copy'
  directory GET::SOURCE_DIR
  file GET::SOURCE_DIR do
    sh "cd #{GET::SOURCE_DIR} && git clone #{GET::REPO} -b #{GET::VERSION}"
  end

  desc 'Update the local copy'
  task :pull do
    sh "cd #{GET::SOURCE_DIR} && git fetch && git checkout #{GET::VERSION}"
  end

  desc 'Remove unnecessary files'
  task :clean do
    %w[GET::SOURCE_DIR].each { |dir| rm_rf dir }
  end

  desc 'Install everything'
  task install: %w[
    get:pull
    get:terraform:init_all
    get:terraform:apply_all
    get:ansible:install
    get:ansible:environment
  ]

  # This does not destroy the shared terraform playbook as we want to keep the dns zone in place.
  desc 'Apply everything'
  multitask apply: %w[get:terraform:primary:apply get:terraform:secondary:apply]

  # This does not destroy the shared terraform playbook as we want to keep the dns zone in place.
  desc 'Destroy everything'
  multitask destroy: %w[get:terraform:primary:destroy get:terraform:secondary:destroy get:gcs:clear_known_hosts]

  desc 'Sets the TOKEN environment variable'
  task :set_token do
    Op.new.gitlab_token
  end

  desc 'Create seed projects'
  task :projects do
    sh GET.host_runner_cmd('ibaum-get-1k-primary-gitlab-rails-1', 'get/files/make-projects ')
  end

  desc 'Fill with gitlab-shells'
  task :saturate do
    Dir.mktmpdir do |dir|
      `git clone https://gitlab.com/gitlab-org/gitlab-shell.git #{File.join(dir, 'gitlab-shell')}`
      threads = []

      ('a'..'k').each do |l|
        threads << Thread.new do
          push(l, dir)
        end
      end

      threads.each(&:join)
    end
  end

  namespace :feature_flag do
    desc 'Check feature flag'
    task :check do
      sh "echo 'puts Feature.enabled?(:geo_job_artifact_replication)' | " \
         'gcloud compute ssh ibaum-get-1k-primary-gitlab-rails-1 -- sudo gitlab-rails runner -'
    end

    desc 'Enable feature flag'
    task :enable do
      sh "echo 'puts Feature.enable(:geo_job_artifact_replication)' | " \
         'gcloud compute ssh ibaum-get-1k-primary-gitlab-rails-1 -- sudo gitlab-rails runner -'
    end

    desc 'Disable feature flag'
    task :disable do
      sh "echo 'puts Feature.disable(:geo_job_artifact_replication)' | " \
         'gcloud compute ssh ibaum-get-1k-primary-gitlab-rails-1 -- sudo gitlab-rails runner -'
    end
  end

  desc 'Run qa against the instance'
  task :qa do
    ENV['GITLAB_URL'] = 'http://primary.get.ibaum.gogitlab.xyz'
    ENV['PASSWORD'] = 'LeTmEiN123!'
    ENV['GITLAB_QA_ACCESS_TOKEN'] = 'Q3oJ45syRdBg7fdCVQrm'
    Rake::Task['qa:run'].invoke
  end

  desc 'Run Geo QA against the instance'
  task :geo_qa do
    ENV['GITLAB_QA_OPTS'] = '--primary-address https://primary.get.ibaum.gogitlab.xyz --secondary-address https://secondary.get.ibaum.gogitlab.xyz'
    ENV['GITLAB_USERNAME'] = 'gitlab-qa'
    ENV['GITLAB_PASSWORD'] = 'imagitlabqauser'
    # ENV['QA_SCENARIO'] = 'Test::Instance::Geo'
    ENV['QA_SCENARIO'] = 'Test::Instance::StagingRefGeo'
    ENV['GITLAB_QA_ACCESS_TOKEN'] = 'glpat-UoyN2ubrYPCMbzy3owQh'
    ENV['QA_IMAGE'] = 'dev.gitlab.org:5005/gitlab/omnibus-gitlab/gitlab-ee-qa:15.0-0375d1ce041'
    sh 'cd /Users/ibaum/src/gdk/primary/gitlab/qa && bundle install && bundle exec bin/qa QA::EE:Scenario::Test::Geo ${GITLAB_QA_OPTS}'
    # sh "gitlab-qa ${QA_SCENARIO} ${RELEASE} ${GITLAB_QA_OPTS}"
  end

  desc 'Generate the terraform and ansible configs'
  task :config do
    sh 'jsonnet --ext-str-file env=env.json -m get/inventories/ibaum-get-geo/ -Sc jsonnet-configs/ansible.jsonnet'
    sh 'jsonnet --ext-str-file env=env.json -m get/terraform/variables/ -Sc jsonnet-configs/terraform.jsonnet'
  end

  namespace :ansible do
    desc 'Ensure we have everything installed'
    task :prep do
      python_version = '3.9.13'
      sh 'asdf plugin list | grep -q python || asdf plugin add python'
      sh "asdf install python #{python_version}"
      sh "asdf global python #{python_version}"
      sh 'pip install -U pip'
      sh <<~COMMAND
        LDFLAGS='-L/opt/homebrew/opt/openssl@1.1/lib' CFLAGS='-I/opt/homebrew/opt/openssl@1.1/include' pip install -r #{GET::ANSIBLE_DIR}/requirements/requirements.txt
      COMMAND
      sh 'asdf reshim python'
      sh "ansible-galaxy install -r #{GET::ANSIBLE_DIR}/requirements/ansible-galaxy-requirements.yml"
    end

    desc 'Upgrade the whole lot'
    task :upgrade do
      sh GET.ansible_cmd("#{GET::INVENTORY_DIR}/all", 'zero_downtime_update.yml', vaultfile: GET::VAULT_FILE)
    end

    # Some things should happen only on the primary
    namespace :primary do
      desc 'Just install the license'
      task :license do
        sh GET.ansible_cmd(
          "#{GET::INVENTORY_DIR}/primary",
          'all.yml',
          extra: '--tags license',
          vaultfile: GET::VAULT_FILE
        )
      end
    end

    desc 'Create the user environment'
    task :environment do
      sh GET.ansible_cmd(["#{GET::INVENTORY_DIR}/primary", "#{GET::INVENTORY_DIR}/secondary"], "#{GET::DIR}/playbooks/environment.yml")
    end

    %w[primary secondary].each do |inst|
      namespace inst do
        desc 'List hosts'
        task :list do
          sh GET.ansible_cmd("#{GET::INVENTORY_DIR}/#{inst}", 'all.yml', extra: '--list-hosts')
        end

        desc "Install on the #{inst} site"
        task :install do
          command = GET.ansible_cmd("#{GET::INVENTORY_DIR}/#{inst}", 'all.yml', vaultfile: GET::VAULT_FILE)
          if ENV.key?('DEBUG') && ENV['DEBUG']
            puts command
          else
            sh command
          end
        end

        desc "Run all on a specific host in #{inst}"
        task :all_on_host do
          sh GET.ansible_cmd(
            "#{GET::INVENTORY_DIR}/#{inst}",
            'all.yml',
            vaultfile: GET::VAULT_FILE,
            extra: "-l '#{ENV.fetch('HOSTS')}'"
          )
        end

        desc 'Install runner on hosts'
        task :runner do
          sh GET.ansible_cmd("#{GET::INVENTORY_DIR}/#{inst}", "#{GET::DIR}/playbooks/runner.yml")
        end

        desc "Ran a command on all hosts in #{inst}"
        task :run_command do
          hosts = ENV.fetch('HOSTS', '*')
          command = ENV.fetch('COMMAND')
          ansible_shell("#{GET::INVENTORY_DIR}/#{inst}", hosts, command)
        end

        desc 'Apply patch from an MR'
        task :patch do
          sh GET.ansible_cmd("#{GET::INVENTORY_DIR}/#{inst}", "#{GET::DIR}/playbooks/patch.yml", vaultfile: GET::VAULT_FILE)
        end
      end
    end

    desc 'Set the password environment variable'
    task :vault_password do
      if GET.prompt_for_password?
        print 'Please enter your ansible vault password: '
        pass = $stdin.getpass.chomp
        ENV['VAULT_PASSWORD'] = pass
        raise 'Invalid password' unless GET.valid_password?
      end
    end

    desc 'Install the whole lot'
    task install: %w[get:ansible:install_all get:ansible:geo:install]

    desc 'Run all on both'
    multitask install_all: %w[get:ansible:primary:install get:ansible:secondary:install]

    namespace :geo do
      desc 'Install geo'
      task :install do
        sh GET.ansible_cmd(
          ["#{GET::INVENTORY_DIR}/primary", "#{GET::INVENTORY_DIR}/secondary"], 'gitlab_geo.yml', vaultfile: GET::VAULT_FILE
        )
      end
    end

    desc 'Run CMD on all machines'
    task :run_on_all do
      sh "cd #{GET::ANSIBLE_DIR} && GCP_AUTH_KIND=application ansible -i inventories/ibaum-get-geo/all -m setup -a 'filter=packages' '*'"
    end
  end

  namespace :gcs do
    desc 'Empties out the google compute known hosts file'
    task :clear_known_hosts do
      File.truncate(File.join(Dir.home, '.ssh', 'google_compute_known_hosts'), 0)
    end
  end

  namespace :hacks do
    desc 'Copy the unified url cert to the secondary'
    task :certcopy do
      sh <<~COMMAND
        (gcloud compute ssh ibaum-get-1k-primary-haproxy-external-1 --command='bash -i -c env' -- -t sudo tar -cvf - /etc/letsencrypt/{live,archive}/gitlab.get.ibaum.gogitlab.xyz/) > foo.tar.gz
      COMMAND
    end
  end

  namespace :terraform do
    desc 'Apply everything'
    task apply_all: %w[get:terraform:apply_envs get:terraform:shared:apply]

    desc 'build envs'
    multitask apply_envs: %w[get:terraform:primary:apply get:terraform:secondary:apply]

    desc 'init all'
    task init_all: %w[set_token parallel_init_all]

    desc 'Set token'
    task :set_token do
      ENV['TOKEN'] = GET.personal_access_token
    end

    desc 'parallel init all'
    multitask parallel_init_all: GET::TERRAFORMS.map { |x| "get:terraform:#{x}:init" }

    desc 'Install the needful'
    task :prep do
      sh 'asdf plugin list | grep -q terraform || asdf plugin add terraform'
      sh "asdf install terraform #{GET::Terraform::VERSION}"
      sh "asdf local terraform #{GET::Terraform::VERSION}"
    end

    GET::TERRAFORMS.each do |inst|
      namespace inst do
        desc "Initialize the #{inst} config"
        task :init do
          Tempfile.open do |f|
            f.write GET::Terraform.extra_cmd(inst)
            f.flush
            sh GET::Terraform.init(inst, f.path) do |ok, _res|
              sh GET::Terraform.init(inst, f.path, upgrade: true) unless ok
            end
          end
        end

        task :mkinit do
          puts GET::Terraform.extra_cmd(inst)
        end

        desc "Show the plan for the current state of the #{inst} configuration"
        task :plan do
          sh GET::Terraform.cmd('plan', inst)
        end

        desc "Apply the current state for the #{inst} configuration"
        task :apply do
          ENV['TF_VAR_ssh_public_key_file'] = 'get/keys/ibaum_get_ssh.pub'
          sh GET::Terraform.apply(inst)
          # end
        end

        desc "Validate the #{inst} terraform files"
        task :validate do
          sh GET::Terraform.cmd('validate', inst)
        end

        desc "Destroy everything terraform related for #{inst}"
        task :destroy do
          sh GET::Terraform.cmd('destroy', inst, extra: '-auto-approve')
        end

        desc "Refresh when outputs are added for #{inst}"
        task :refresh do
          sh GET::Terraform.cmd('refresh', inst)
        end

        desc "Show the outputs for #{inst}"
        task :output do
          sh GET::Terraform.cmd('output', inst)
        end

        desc "show the current state of #{inst}"
        task :show do
          sh GET::Terraform.cmd('show', inst)
        end

        desc "manage the providers of #{inst}"
        task :providers do
          sh GET::Terraform.cmd('providers', inst)
        end
      end
    end
  end
end
