namespace :rvm do
  namespace :setup do
    task :qa do
      sh "rvm use ruby_version && rvm gemset create #{QA_RVM}" unless system("rvm #{RVM_RUBY_VERSION}@#{QA_RVM} do ruby -v > /dev/null 2>&1")

      unless system("rvm #{RVM_RUBY_VERSION}@#{QA_RVM} do gitlab-qa --help > /dev/null")
        sh "rvm #{RVM_RUBY_VERSION}@#{QA_RVM} do gem install gitlab-qa"
      end
    end
  end
end
