namespace :geo do
  instances =%i[primary secondary] 
  desc 'Download everywhere'
  multitask download: instances.map { |i| "geo:#{i}:download:com" }

  instances.each do |inst|
    namespace inst do
      namespace :download do
        desc "Download a deb from gitlab.com pipeline to the #{inst} site"
        task :com do
          job_num = job(ENV.fetch('JOB'))
          raise 'Provide JOB please' if job_num.nil?

          gitlab_token = ENV.fetch('TOKEN')
          url = "https://gitlab.com/api/v4/projects/14588374/jobs/#{job_num}/artifacts/pkg/ubuntu-focal/gitlab.deb"
          output = "gitlab-#{job_num}.deb"
          cmd = "curl --header 'Private-Token: #{gitlab_token}' -o #{output} -L #{url}"
          # sh %(ssh #{inst}.get.ibaum.gogitlab.xyz "#{cmd}")
          host_tag = "ibaum-get-1k-#{inst}-gitlab-rails"
          # ip = `gcloud compute instances list --filter="tags:#{host_tag}" --format="table[no-heading](networkInterfaces[0].accessConfigs[0].natIP)"`
          ip = `gcloud compute instances list --filter="tags:#{host_tag}" --format="table[no-heading](name)"`.chomp
          sh %(gcloud compute ssh #{ip} -- "#{cmd}")
        end

        desc "Download a deb from dev.gitlab.org pipeline to the #{inst} site"
        task :dev do
          job = job(ENV.fetch('JOB'))
          url = "https://dev.gitlab.org/api/v4/projects/gitlab%2Fomnibus-gitlab/jobs/#{job}/artifacts"
          cmd = "curl #{url} -o gitlab-#{job}.zip -L --header 'Private-Token: #{dev_gitlab_token}'"
          sh %(ssh #{inst}.ibaum.gogitlab.xyz "#{cmd}")
          sh %(ssh #{inst}.ibaum.gogitlab.xyz "unzip gitlab-#{job}.zip '*.deb'")
        end
      end
    end
  end
end
