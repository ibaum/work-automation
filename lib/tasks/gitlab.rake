namespace :gitlab do
  desc 'Get my profile data'
  task :profile do
    from = Date.parse(ENV.fetch('FROM', '2016-10-16'))
    to = Date.parse(ENV.fetch('TO', Date.today))

    gl = Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: gitlab_token)
    user = gl.user

    events = gl.user_events(user.id, before: to.to_s, after: from.to_s)

    events.auto_paginate do |event|
      next unless event.action_name.eql?('commented on')

      pp event.target_title
      pp event.note.body
      pp '---'
    end
  end

  desc 'Get MRs'
  task :mrs do
    gl = Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: gitlab_token)

    # go = gl.group('gitlab-org')
    # go.merge_requests().auto_paginate do |mr|
    gl.group_merge_requests(
      'gitlab-org',
      { state: 'opened', labels: ['workflow::ready for review', 'group::distribution'] }
    ).auto_paginate do |mr|
      puts "#{mr.title} - #{mr.web_url}"
    end
  end
end
