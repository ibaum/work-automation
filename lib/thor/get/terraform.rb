class Terraform < Thor
  include Thor::Actions

  SITES = %w[primary secondary shared].freeze

  class_option :site,
               banner: 'terraform site to run against',
               desc: 'The terraform site to run against. One of primary, secondary, or shared',
               default: 'primary'

  class_option :all, type: :boolean, banner: 'run against all sites'

  no_commands do
    def find_sites(options)
      if options[:all]
        SITES
      else
        [options[:site]]
      end
    end
  end

  desc 'apply', 'Apply the current state for the primary configuration'
  def apply
    ENV['TF_VAR_ssh_public_key_file'] = 'get/keys/ibaum_get_ssh.pub'
    run(GET::Terraform.apply(options[:site]))
  end

  desc 'destroy', 'Destroy everything terraform related for primary'
  def destroy
    run(GET::Terraform.cmd('destroy', inst, extra: '-auto-approve'))
  end

  desc 'init', 'Initialize the primary config'
  def init
    Tempfile.open do |f|
      f.write GET::Terraform.extra_cmd(options[:site])
      f.flush
      run GET::Terraform.init(options[:site], f.path) do |ok, _res|
        run GET::Terraform.init(options[:site], f.path, upgrade: true) unless ok
      end
    end
  end

  desc 'init_upgrade', 'Upgrade the terraform config'
  def init_upgrade
    sites = find_sites(options)
    sites.each do |site|
      Tempfile.open do |f|
        f.write GET::Terraform.extra_cmd(site)
        f.flush
        run GET::Terraform.init(site, f.path, upgrade: true)
      end
    end
  end

  desc 'output', 'Show the outputs for primary'
  def output
    puts 'NI: apply'
  end

  desc 'plan', 'Show the plan for the current state of the primary configuration'
  def plan
    # inside("get/terraform/#{options[:site]}") do #TODO
    run(GET::Terraform.cmd('plan', options[:site]))
    # end
  end

  desc 'providers', 'manage the providers of primary'
  def providers
    puts 'NI: apply'
  end

  desc 'refresh', 'Refresh when outputs are added for primary'
  def refresh
    puts 'NI: apply'
  end

  desc 'show', 'show the current state of primary'
  def show
    puts 'NI: apply'
  end

  desc 'validate', 'Validate the primary terraform files'
  def validate
    puts 'NI: apply'
  end
end

# desc 'apply_all', 'Apply everything'
# def apply_all
# puts 'NI: apply_all'
# end

# desc 'apply_envs', 'build_envs'
# def apply_envs
# puts 'NI: apply_envs'
# end

# desc 'init_all', 'init all'
# def init_all
# puts 'NI: init_all'
# end

# desc 'parallel_init_all', 'parallel init all'
# def parallel_init_all
# puts 'NI: parallel_init_all'
# end

# desc 'prep', 'Install the needful'
# def prep
# puts 'NI: prep'
# end

# SITES.each do |site|
# desc site, "Run commands on #{site} site"
# subcommand site, Site
# end
