class Ansible < Thor

  no_commands do
    def load_rake
      rake = Rake::Application.new
      Rake.application = rake
      rake.init 'doit'
      rake.load_rakefile # ('Rakefile')
      # Rake.load_rakefile('Rakefile')
    end
  end

  desc 'all_on_host', 'Run all on a specific host in primary'
  def all_on_host
    puts 'NI: all_on_host'
  end

  desc 'install', 'Run install on site'
  method_option :site, type: :string, default: 'all'
  def install
    load_rake
    if options[:site].eql?('all')
      Rake::Task['get:ansible:install'].invoke
    else
      Rake::Task["get:ansible:#{options[:site]}:install"].invoke
    end
  end

  desc 'license', 'Just install the license'
  def license
    puts 'NI: license'
  end

  desc 'list', 'List hosts'
  def list
    puts 'NI: list'
  end

  desc 'patch', 'Apply patch from an MR'
  def patch
    puts 'NI: patch'
  end

  desc 'run', 'Ran a command on all hosts in primary'
  def run_command
    puts 'NI: run'
  end

  desc 'runner', 'Install runner on hosts'
  def runner
    puts 'NI: runner'
  end

  desc 'environment', 'Create the user environment'
  def environment
    puts 'NI: environment'
  end

  desc 'prep', 'Ensure we have everything installed'
  def prep
    puts 'NI: prep'
  end

  desc 'run_on_all', 'Run CMD on all machines'
  def run_on_all
    puts 'NI: run_on_all'
  end

  desc 'upgrade', 'Upgrade the whole lot'
  def upgrade
    puts 'NI: upgrade'
  end

  desc 'vault_password', 'Set the password environment variable'
  def vault_password
    puts 'NI: vault_password'
  end
end
