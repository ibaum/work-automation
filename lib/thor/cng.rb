class Cng < Thor
  include Thor::Actions

  def self.exit_on_failure?
    true
  end

  COMMON_VALUES = {
    'global' => {
      'postgresql' => {
        'install' => false
      },
      'prometheus' => {
        'install' => false
      },
      'certmanager-issuer' => {
        'email' => 'foo@bar.com'
      },
      'gitlab-runner' => {
        'install' => false
      }
    },
    'sidekiq' => {
      'maxReplicas' => 1
    },
    'webservice' => {
      'maxreplicacount' => 1
    },
    'gitlab-shell' => {
      'minReplicas' => 1,
      'maxReplicas' => 1
    }
  }.freeze
  CONTEXT = 'rancher-desktop'.freeze

  no_commands do
    def kubectl
      "kubectl --context #{CONTEXT}"
    end

    def helm
      "helm --kube-context=#{CONTEXT}"
    end

    def copy_secret(from_ns, to_ns, secret)
      "#{kubectl} get --namespace #{from_ns} -o yaml secret #{secret} | grep -v '^\s*namespace:\s' | #{kubectl} apply --namespace #{to_ns} --overwrite=true -f -"
    end

    def helm_uninstall(namespace, release)
      "(#{helm} --namespace #{namespace} get values #{release} > /dev/null && #{helm} --namespace #{namespace} uninstall #{release}) || true"
    end
  end

  desc 'install', 'install using helm'
  def install
    invoke :namespace
    invoke :postgresql
    invoke :primary
    invoke :secondary
  end

  desc 'namespace', 'create the namespaces'
  def namespace
    %w[primary secondary].each do |name|
      run "#{kubectl} get namespace gitlab-#{name} || #{kubectl} create namespace gitlab-#{name}"
    end
  end

  desc 'primary', 'install the primary helm chart'
  def primary
    values = {
      'global' => {
        'hosts' => {
          'domain' => 'primary.localdev.me'
        },
        'psql' => {
          'host' => 'main-postgresql-primary-0.main-postgresql-primary-hl.default.svc.cluster.local',
          'username' => 'postgres',
          'password' => {
            'secret' => 'main-postgresql',
            'key' => 'postgres-password'
          }
        },
        'geo' => {
          'nodeName' => 'Primary Site',
          'enabled' => true,
          'role' => 'primary'
        }
      }
    }
    Tempfile.create('values.yaml') do |file|
      file.write COMMON_VALUES.merge(values).to_yaml
      file.fsync
      run "helm --kube-context #{CONTEXT} upgrade --install primary gitlab/gitlab --namespace gitlab-primary  -f #{file.path}"
    end
  end

  desc 'postgresql', 'setup postgresql instances'
  def postgresql
    values = {
      'main' => {
        'architecture' => 'replication',
        'replication' => {
          'numSynchronousReplicas' => 1
        },
        'image' => {
          'tag' => '14.10.0'
        },
        'auth' => {
          'database' => 'gitlabhq_production'
        }
      },
      'tracking' => {
        'image' => {
          'tag' => '14.10.0'
        },
        'auth' => {
          'database' => 'gitlabhq_geo_production'
        }
      }
   }

    %w[main tracking].each do |inst|
      Tempfile.create('values.yaml') do |file|
        file.write values[inst].to_yaml
        file.fsync
        run "helm --kube-context #{CONTEXT} upgrade --install #{inst} oci://registry-1.docker.io/bitnamicharts/postgresql -f #{file.path}"
      end
    end
    run copy_secret('default', 'gitlab-primary', 'main-postgresql')
    run copy_secret('default', 'gitlab-secondary', 'main-postgresql')
    run copy_secret('default', 'gitlab-secondary', 'tracking-postgresql')
  end

  desc 'uninstall', 'Remove the installation'
  def uninstall
    %w[main tracking gitlab-primary gitlab-secondary].each do |inst|
      run helm_uninstall('default', inst)
    end

    run "#{kubectl} delete namespace gitlab-primary"
    run "#{kubectl} delete namespace gitlab-secondary"
    run "#{kubectl} delete pvc -l app.kubernetes.io/instance=tracking"
    run "#{kubectl} delete pvc -l app.kubernetes.io/instance=main"
  end

  desc 'secondary', 'Install the secondary'
  def secondary
    values = {
      'global' => {
        'hosts' => {
          'domain' => 'secondary.localdev.me'
        },
        'psql' => {
          'host' => 'main-postgresql-read-0.main-postgresql-read-hl.default.svc.cluster.local',
          'username' => 'postgres',
          'port' => 5432,
          'password' => {
            'secret' => 'main-postgresql',
            'key' => 'postgres-password'
          }
        },
        'geo' => {
          'enabled' => true,
          'role' => 'secondary',
          'nodeName' => 'Secondary Site',
          'psql' => {
            'host' => 'tracking-postgresql-0.tracking-postgresql-hl.default.svc.cluster.local',
            'username' => 'postgres',
            'port' => 5432,
            'password' => {
              'secret' => 'tracking-postgresql',
              'key' => 'postgres-password'
            }
          }
        }
      }
    }

    %w[gitlab-shell-host-keys rails-secret registry-secret].each do |secret|
      run copy_secret('gitlab-primary', 'gitlab-secondary', "primary-#{secret}")
    end

    Tempfile.create('values.yaml') do |file|
      file.write COMMON_VALUES.merge(values).to_yaml
      file.fsync
      run "helm --kube-context #{CONTEXT} upgrade --install secondary gitlab/gitlab --namespace gitlab-secondary -f #{file.path}"
    end
  end
end
