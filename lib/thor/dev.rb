require_relative '../get'

require 'thor'

class Dev < Thor
  include Thor::Actions

  desc 'setup', 'setup the dev instance'
  def setup
    run GET.ansible_cmd("../../../../playbooks/dev.gcp.yml", "#{GET::DIR}/playbooks/dev.yml")
  end
end
