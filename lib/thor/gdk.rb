require 'thor'

class Gdk < Thor
  include Thor::Actions

  GDK_BASE_DIR = "#{Dir.home}/src/gdk".freeze
  INSTANCES = %w[primary secondary].freeze

  no_commands do
    def run_in_gdk(command)
      INSTANCES.each do |inst|
        inside "#{GDK_BASE_DIR}/#{inst}" do
          run command, verbose: false
        end
      end
    end
  end

  desc 'compare', 'Compare gitlab revisions'
  def compare
    INSTANCES.each do |inst|
      inside "#{GDK_BASE_DIR}/#{inst}" do
        run 'git rev-parse HEAD', verbose: false
      end
    end
  end

  desc 'start', 'Start both instances'
  def start
    INSTANCES.each do |inst|
      inside "#{GDK_BASE_DIR}/#{inst}" do
        run 'gdk start', verbose: false
      end
    end
  end

  desc 'geo_status', 'Check geo status'
  def geo_status
    inside "#{GDK_BASE_DIR}/secondary/gitlab" do
      run 'bin/rails geo:status'
    end
  end

  desc 'restart', 'Restart both instances'
  def restart
    INSTANCES.each do |inst|
      inside "#{GDK_BASE_DIR}/#{inst}" do
        run "GDK_DEBUG=1 gdk restart"
      end
    end
  end

  desc 'cleanup_logs', 'Truncate gdk logs'
  def cleanup_logs
    run_in_gdk('GDK_CLEANUP_CONFIRM=false GDK_CLEANUP_SOFTWARE=false gdk cleanup')
  end
end
