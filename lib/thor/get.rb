require_relative '../get'
require_relative 'get/terraform'
require_relative 'get/ansible'

require 'thor'
require 'thor/rake_compat'

class Get < Thor
  include Thor::Actions

  DEFAULT_SIZE = '1k'.freeze

  class_option :size, default: DEFAULT_SIZE, desc: 'Size of the installation'
  class_option :config, default: 'env.json', desc: 'jsonnet config file'
  class_option :site, desc: 'Site to destroy', type: :string, default: 'all'

  def self.exit_on_failure?
    true
  end

  no_commands do
    def load_rake
      rake = Rake::Application.new
      Rake.application = rake
      rake.init 'doit'
      rake.load_rakefile # ('Rakefile')
      # Rake.load_rakefile('Rakefile')
    end
  end

  desc 'terraform', 'terraform subcommands'
  subcommand 'terraform', Terraform

  desc 'ansible', 'ansible subcommands'
  subcommand 'ansible', Ansible

  desc 'config', 'Generate the config from json (env.json by default)'
  def config
    run "jsonnet --ext-str-file env=#{options[:config]} -m get/inventories/ibaum-get-geo/ -Sc jsonnet-configs/ansible.jsonnet"
    run "jsonnet --ext-str-file env=#{options[:config]} -m get/inventories/terraform/ -Sc jsonnet-configs/terraform.jsonnet"
  end

  desc 'config_toml', 'Generate the config from toml (env.toml by default)'
  def config_toml
    path = File.join(File.dirname(__FILE__), '../../', 'env.toml')
    Work::Config.new(path).generate
  end

  desc 'install', 'Install everything'
  def install
    load_rake # TODO: setting size manually cases this to fail with "invalid option: --size"
    ENV['SIZE'] = options[:size]
    ENV['RIA_VAULT_PASS'] = `scripts/get-ansible-vault-pass`
    # invoke :pull, [] # TODO: failing since it passes options to the invoked
    invoke :config
    # Rake::Task['get:install'].invoke
    # TODO: After terraform:apply_all, wait for machines to be ready
    %w[get:terraform:init_all get:terraform:apply_all get:ansible:environment get:ansible:install].each do |task|
      Rake::Task[task].invoke
    end
  end

  desc 'list', 'list some stuff'
  def list
    load_rake
    Rake::Task['tasks'].invoke
  end

  desc 'destroy', 'Destroy everything'
  method_option :site, desc: 'Site to destroy', type: :string, default: 'all'
  def destroy
    load_rake
    ENV['SIZE'] = options[:size]
    if options[:site].eql?('all')
      Rake::Task['get:destroy'].invoke
    else
      Rake::Task["get:terraform:#{options[:site]}:destroy"].invoke
    end
  end

  desc 'patch', 'patch the instance with the noted MR'
  option :mr, desc: 'MR to pull the patch from'
  def patch
    run GET.ansible_cmd(
      %w[primary secondary].map { |inst| "#{GET::INVENTORY_DIR}/#{inst}" },
      "#{GET::DIR}/playbooks/patch.yml",
      vaultfile: GET::VAULT_FILE, extra: "-e mr=#{options[:mr]}"
    )
  end

  desc 'pgupgrade', 'run pg-upgrade'
  def pgupgrade
  inventories = if options[:site].eql?('all')
                  %w[primary secondary].map { |inst| "#{GET::INVENTORY_DIR}/#{inst}" }
                else
                  "#{GET::INVENTORY_DIR}/#{options[:site]}" 
                end
    run GET.ansible_cmd(
      inventories,
      "#{GET::DIR}/playbooks/pg_upgrade.yml",
      vaultfile: GET::VAULT_FILE
    )
  end

  desc 'pull', 'pull the appropriate version of get'
  def pull
    run "cd #{GET::SOURCE_DIR} && git fetch && git checkout #{GET::VERSION}"
  end

  desc 'revert_pgupgrade', 'revert pg-upgrade'
  def revert_pgupgrade
    run GET.ansible_cmd(
      %w[primary secondary].map { |inst| "#{GET::INVENTORY_DIR}/#{inst}" },
      "#{GET::DIR}/playbooks/pg_upgrade-reset.yml",
      vaultfile: GET::VAULT_FILE, extra: "-l ibaum-get-secondary-postgres-1"
    )
  end
end
