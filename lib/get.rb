require_relative 'get/terraform'
require_relative 'op'

class GET
  GOOGLE_AUTH = 'GCP_AUTH_KIND=serviceaccount GCP_SERVICE_ACCOUNT_FILE=../../../../get/keys/ibaum-automation.json'.freeze
  ANSIBLE_CMD = "#{GOOGLE_AUTH} ansible".freeze
  DIR = File.join(File.dirname(File.absolute_path(__FILE__)), '..').freeze
  PLAYBOOK_CMD = "#{GOOGLE_AUTH} ansible-playbook".freeze
  REPO = 'git@gitlab.com:gitlab-org/gitlab-environment-toolkit'.freeze
  BASE_SOURCE_DIR = File.join(DIR, 'tmp/source').freeze
  VERSION = '3.0.2'.freeze
  SOURCE_DIR = File.join(BASE_SOURCE_DIR, 'gitlab-environment-toolkit').freeze
  ANSIBLE_DIR = File.join(SOURCE_DIR, 'ansible').freeze
  INVENTORY_DIR = File.join(DIR, 'get/inventories/ibaum-get-geo').freeze
  VAULT_FILE = File.join(INVENTORY_DIR, 'secrets.yml').freeze
  TERRAFORMS = %w[primary secondary shared].freeze
  VAULT_SECRET = File.join(DIR, 'vault-secret')
  ZONE = 'us-central1-b'.freeze

  class << self
    def prompt_for_password?
      !ENV.key?('VAULT_PASSWORD')
    end

    def personal_access_token
      return ENV['PAT'] if ENV.key?('PAT')
      return ENV['TOKEN'] if ENV.key?('TOKEN')

      o = Op.new
      o.gitlab_token
    end

    def valid_password?
      system("ansible-vault view --vault-password-file #{VAULT_SECRET} #{VAULT_FILE} > /dev/null")
    end

    def ansible_cmd(inventory, playbook, vaultfile: nil, extra: nil)
      command = "cd #{ANSIBLE_DIR} && #{PLAYBOOK_CMD} #{inventory_arg(inventory)} #{playbook_file(playbook)}"
      # command << " -e @#{vaultfile}  --vault-password-file #{VAULT_SECRET}" unless vaultfile.nil?
      command << " -e @#{vaultfile}  --vault-password-file ../../../../scripts/get-ansible-vault-pass" unless vaultfile.nil?
      command << " #{extra}" unless extra.nil?
      command << " --tags #{ENV.fetch('TAGS')}" if ENV.key?('TAGS')
      command << ' -vvvv' if ENV.key?('VERBOSE')

      command
    end

    def inventory_arg(inventory)
      if inventory.instance_of?(String)
        "-i #{inventory}"
      elsif inventory.instance_of?(Array)
        "-i #{inventory.join(' -i ')}"
      end
    end

    def playbook_file(playbook)
      if File.exist?(playbook)
        playbook
      else
        "playbooks/#{playbook}"
      end
    end

    def ansible_shell(inventory, hosts, command)
      command = cd "#{GET_ANSIBLE_DIR} && #{ANSIBLE_CMD} -i #{inventory} #{hosts} -a '#{command}'"
      sh command
    end

    def size
      ENV.key?('SIZE') ? ENV['SIZE'] : '1k'
    end

    def host_runner_cmd(host, cmdfile)
      "cat #{cmdfile} | gcloud compute ssh --zone #{ZONE} #{host} -- sudo gitlab-rails runner -"
    end
  end
end
