require 'tty-command'

class Op
  attr_accessor :session

  def token(token_id)
    `op item get "#{token_id}" --fields notesPlain`.chomp
  end

  def gitlab_token
    return ENV['TOKEN'] if ENV.key?('TOKEN')

    t = token('GitLab Personal Token')
    ENV['TOKEN'] = t
    t
  end

  def dev_gitlab_token
    token('dev.gitlab.org access token')
  end
end
