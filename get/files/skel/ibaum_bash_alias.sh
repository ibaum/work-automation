alias diff='diff -urw'
alias gc='sudo gitlab-ctl'
alias ge='sudo -e /etc/gitlab/gitlab.rb'
alias gp='sudo gitlab-psql'
alias gr='sudo gitlab-ctl reconfigure'
alias grails='sudo gitlab-rails'
alias grake='sudo gitlab-rake'

set -o vi
export EDITOR=vim
