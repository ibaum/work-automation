variable "prefix" {
  type = string
}

variable "project" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "geo_site" {
  type = string
}

variable "geo_deployment" {
  type = string
}

variable "ssh_public_key_file" {
  type = string
}

variable "machine_image" {
  type = string
}

variable "object_storage_buckets_force_destroy" {
  type = bool
}
