terraform {
  backend "http" {
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_compute_address" "external_ip" {
  name = "${var.prefix}-external"
  region = var.region
}

output "external_ip_addr" {
  value = google_compute_address.external_ip.address
}
