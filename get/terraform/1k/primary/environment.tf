module "gitlab_ref_arch_gcp" {
  source = "../../../../tmp/source/gitlab-environment-toolkit/terraform/modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project

  service_account_prefix = "ibaum-pri"

  geo_site = var.geo_site
  geo_deployment = var.geo_deployment

  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-highcpu-8"

  # monitor_node_count = 1
  # monitor_machine_type = "n1-highcpu-4"

  haproxy_external_node_count = 1
  haproxy_external_machine_type = "n1-highcpu-2"
  haproxy_external_external_ips = [google_compute_address.external_ip.address]

  machine_image = var.machine_image

  haproxy_internal_node_count = 1
  haproxy_internal_machine_type = "n1-highcpu-2"
  
  consul_node_count = 3
  consul_machine_type = "n1-highcpu-2"

  pgbouncer_node_count = 1
  pgbouncer_machine_type = "n1-highcpu-2"

  postgres_node_count = 3
  postgres_machine_type = "n1-standard-4"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
