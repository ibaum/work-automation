variable "prefix" {
  default = "ibaum-get-3k-primary"
}

variable "project" {
  default = "group-geo-f9c951"
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-b"
}

variable "geo_site" {
  default = "geo-primary-site"
}

variable "geo_deployment" {
  default = "ibaum-geo"
}

variable "ssh_public_key_file" {
  default = "../keys/distribution/ibaum_get_ssh.pub"
}

variable "machine_image" {
  default = "ubuntu-2004-lts"
}

variable "object_storage_buckets_force_destroy" {
  default = "true"
}

variable "container_registry_enable" {
  default = true
}
