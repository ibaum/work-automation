variable "project" {
  default = "group-geo-f9c951"
}

variable "primary_region" {
  default = "us-central1"
}

variable "secondary_region" {
  default = "us-west1"
}

variable "zone" {
  default = "us-central1-b"
}

variable "primary_ip" {
}

variable "secondary_ip" {

}

# variable "primary_ip_internal" {

# }

# variable "secondary_ip_internal" { 

# }
