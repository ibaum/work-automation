/*
 * For this to work, you need to add a NS record to the gogitlab.xyz role for the managed zone. Values should be the google name servers
 * Once the terraform run is complete
 * 1. go to https://console.cloud.google.com/net-services/dns/zones?project=group-geo-f9c951
 * 2. Click on your newly created terraform zone
 * 3. Click on "Registrar Setup" in the upper right corner and make note of the NS values
 * 4. Go to https://console.cloud.google.com/net-services/dns/zones/gogitlabxyz/details?project=group-geo-f9c951
 * 5. Click on "Add Record Set"
 * 6. Set the "DNS Name" to the name of your terraform zone
 * 7. Set "Resource Record Type" to "NS"
 * 8. For the Name server values, add all of the nameservers from step 3
 * 9. Click create
 * 10. Validate by running "dig.primary.YOURDOMAIN.gogitlab.xyz"
 */

terraform {
  backend "http" {
  }

  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.63.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.primary_region
  zone    = var.zone
}

resource "google_dns_managed_zone" "gogitlab" {
  name = "ibaum-gogitlab-zone"
  dns_name = "get.ibaum.gogitlab.xyz."
}

# resource "google_dns_managed_zone" "internal_gogitlab" {
#   name = "ibaum-gogitlab-zone-internal"
#   dns_name = "get.ibaum.gogitlab.xyz."

#   visibility = "private"

#   private_visiblity_config {
#     networks {
#       network_url = "https://www.googleapis.com/compute/v1/projects/group-geo-f9c951/global/networks/default"
#     }
#   }
# }

resource "google_dns_record_set" "unified" {
  name =  "gitlab.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"

  routing_policy {
    geo {
      location = var.primary_region
      rrdatas = [var.primary_ip]
    }

    geo {
      location = var.secondary_region
      rrdatas = [var.secondary_ip]
    }
  }
}

resource "google_dns_record_set" "unified_registry" {
  name =  "registry.gitlab.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"

  routing_policy {
    geo {
      location = var.primary_region
      rrdatas = [var.primary_ip]
    }

    geo {
      location = var.secondary_region
      rrdatas = [var.secondary_ip]
    }
  }
}

resource "google_dns_record_set" "primary" {
  name = "primary.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"
  rrdatas = [var.primary_ip]
}

resource "google_dns_record_set" "registry" {
  name = "registry.primary.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"
  rrdatas = [var.primary_ip]
}

resource "google_dns_record_set" "secondary" {
  name = "secondary.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"
  rrdatas = [var.secondary_ip]
}

resource "google_dns_record_set" "secondary_registry" {
  name = "registry.secondary.${google_dns_managed_zone.gogitlab.dns_name}"
  managed_zone = google_dns_managed_zone.gogitlab.name
  type = "A"
  rrdatas = [var.secondary_ip]
}

# resource "google_dns_record_set" "primary_internal" {
#   name = "primary.${google_dns_managed_zone.internal_gogitlab.dns_name}"
#   managed_zone = google_dns_managed_zone.gogitlab.name
#   type = "A"
#   rrdatas = [var.primary_ip_internal]
# }

# resource "google_dns_record_set" "secondary_internal" {
#   name = "secondary.${google_dns_managed_zone.internal_gogitlab.dns_name}"
#   managed_zone = google_dns_managed_zone.gogitlab.name
#   type = "A"
#   rrdatas = [var.primary_ip_internal]
# }

output "name_servers" {
  value = google_dns_managed_zone.gogitlab.name_servers
}
