local env = std.parseJson(std.extVar('env'));
local projects = env.projects;
local plugin = 'gcp_compute';
local all_filters = env.all_filters;
local primary_filters = env.primary_filters;
local secondary_filters = env.secondary_filters;

local main_vars = {
    all: {
        vars: {
            ansible_user: env.ansible.user,
            ansible_ssh_private_key_file: env.ansible.ssh_private_key_file,
            cloud_provider: env.cloud_provider,
            external_ssl_source: 'letsencrypt',
            external_ssl_letsencrypt_issuer_email: 'ibaum+get@gitlab.com',
            gitlab_license_file: "../../../../../get/keys/ibaum_ultimate_license",
            system_packages_auto_security_upgrade: false,
            geo_enable_object_storage_replication: false,
            service_account_file: "../../../../../get/keys/ibaum-automation.json",
        }
         } + (
            if std.objectHas(env, 'gitlab_version') then
                { gitlab_version: env.gitlab_version }
             )
};

local primary_vars = main_vars + {
    all+: {
        vars+: {
            prefix: "ibaum-get-primary",
            external_url: env.external_url,
            geo_primary_external_url: env.external_url,
            geo_primary_site_group_name: "geo_primary_site",
            geo_primary_site_name: "Primary Site"
        }
    }
};

local secondary_vars = main_vars + {
    all+: {
        vars+: {
            prefix: "ibaum-get-secondary",
            external_url: env.secondary_external_url,
            geo_secondary_external_url: env.secondary_external_url,
            geo_secondary_site_group_name: "geo_secondary_site",
            geo_secondary_site_name: "Secondary Site"
        }
    }
};

local keyed_groups = [
    { key: 'labels.gitlab_node_type', separator: '' },
    { key: 'labels.gitlab_node_level', separator: '' },
    { key: 'labels.gitlab_geo_site', separator: '' },
    { key: 'labels.gitlab_geo_full_role', separator: '' },
];
local scopes = [ 'https://www.googleapis.com/auth/compute' ];
local hostnames = [ 'name' ];
local compose = { ansible_host: 'networkInterfaces[0].accessConfigs[0].natIP' };

local main_gcp = {
  plugin: plugin,
  projects: projects,
  keyed_groups: keyed_groups,
  scopes: scopes,
  hostnames: hostnames,
  compose: compose
};

{
    // 'all/geo.gcp.yml': std.manifestYamlDoc(
    //         main_gcp + { filters: all_filters },
    //         indent_array_in_object=true, quote_keys=false),
    'primary/geo.gcp.yml': std.manifestYamlDoc(
            main_gcp + { filters: primary_filters },
            indent_array_in_object=true, quote_keys=false),
    'secondary/geo.gcp.yml': std.manifestYamlDoc(
            main_gcp + { filters: secondary_filters },
            indent_array_in_object=true, quote_keys=false),
    // 'all/vars.yml': std.manifestYamlDoc(
    //         main_vars,
    //         indent_array_in_object=true, quote_keys=false),
    'primary/vars.yml': std.manifestYamlDoc(
            primary_vars,
            indent_array_in_object=true, quote_keys=false),
    'secondary/vars.yml': std.manifestYamlDoc(
            secondary_vars,
            indent_array_in_object=true, quote_keys=false),

}
