local env = std.parseJson(std.extVar('env'));

local terraform_env = {
    geo_deployment: env.geo_deployment,
    machine_image: env.machine_image,
    object_storage_buckets_force_destroy: env.object_storage_buckets_force_destroy,
    project: env.projects[0],
    ssh_public_key_file: env.ssh_public_key_file,
};

local terraform_environment = {
    module: {
        gitlab_ref_arch_gcp: {
            source: "../../../../tmp/source/gitlab-environment-toolkit/terraform/modules/gitlab_ref_arch_gcp",
            prefix: "${var.prefix}",
            project: "${var.project}",
            service_account_prefix: "${var.sa_prefix}",
            geo_site: "${var.geo_site}",
            geo_deployment: "${var.geo_deployment}",
            gitlab_rails_node_count: 1,
            gitlab_rails_machine_type: "n1-highcpu-8",
            haproxy_external_node_count: 1,
            haproxy_external_machine_type: "n1-highcpu-2",
            haproxy_external_external_ips: ["${google_compute_address.external_ip.address}"],
            machine_image: "${var.machine_image}",
            haproxy_internal_node_count: 1,
            haproxy_internal_machine_type: "n1-highcpu-2",
            consul_node_count: 3,
            consul_machine_type: "n1-highcpu-2",
            pgbouncer_node_count: 1,
            pgbouncer_machine_type: "n1-highcpu-2",
            postgres_node_count: 3,
            postgres_machine_type: "n1-standard-4"
            //webservice_node_pool_count: 3,
            //webservice_node_pool_machine_type: "n1-highcpu-32",
            //sidekiq_node_pool_max_count: 3,
            //sidekiq_node_pool_machine_type: "n1-standard-4",
            //supporting_node_pool_count: 2,
            //supporting_node_pool_machine_type: "n1-standard-4",
            //consul_node_count: 3,
            //consul_machine_type: "n1-highcpu-2",
            //gitaly_node_count: 1,
            //gitaly_machine_type: "n1-standard-16",
            //haproxy_internal_node_count: 1,
            //haproxy_internal_machine_type: "n1-highcpu-2",
            //monitor_node_count: 1,
            //monitor_machine_type: "n1-highcpu-4",
            //pgbouncer_node_count: 1,
            //pgbouncer_machine_type: "n1-highcpu-2",
            //postgres_node_count: 3,
            //postgres_machine_type: "n1-standard-8",
            //redis_cache_node_count: 1,
            //redis_cache_machine_type: "n1-standard-4",
            //redis_persistent_node_count: 1,
            //redis_persistent_machine_type: "n1-standard-4",
        }
    },
    variable: {
        geo_deployment: {},
        geo_site: {},
        machine_image: {},
        object_storage_buckets_force_destroy: {},
        prefix: {},
        project: {},
        region: {},
        ssh_public_key_file: {},
        sa_prefix: {},
        zone: {}
    },
    output: {
     gitlab_ref_arch_gcp: {
         value: "${module.gitlab_ref_arch_gcp}"
     }
    }
};

local terraform_main = {
    terraform: {
        backend: {
            http: {}
        }
    },
    provider: {
        google: {
            project: "${var.project}",
            region: "${var.region}",
            zone: "${var.zone}"
        }
    },
    resource: {
        google_compute_address: {
            external_ip: {
                name: "${var.prefix}-external",
                region: "${var.region}"
            }
        }
    },
    output: {
        external_ip_addr: {
            value: "${google_compute_address.external_ip.address}"
        }
    }
};

local primary_terraform = {
    region: env.primary_region,
    zone: env.primary_zone,
    geo_site: 'geo-primary-site',
    prefix: env.prefix + '-primary',
    sa_prefix: "ibaum-pri",
} + terraform_env;

local secondary_terraform = {
    region: env.secondary_region,
    zone: env.secondary_zone,
    geo_site: 'geo-secondary-site',
    prefix: env.prefix + '-secondary',
    sa_prefix: 'ibaum-sec',
} + terraform_env;

{
    'primary/environment.tf.json': std.manifestJsonEx(terraform_environment, ' '),
    'primary/main.tf.json': std.manifestJsonEx(terraform_main, ' '),
    'primary/terraform.tfvars.json': std.manifestJsonEx(primary_terraform, '  '),
    'secondary/terraform.tfvars.json': std.manifestJsonEx(secondary_terraform, '  '),
    'secondary/environment.tf.json': std.manifestJsonEx(terraform_environment, ' '),
    'secondary/main.tf.json': std.manifestJsonEx(terraform_main, ' '),
}
