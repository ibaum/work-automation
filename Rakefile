require 'cgi'
require 'date'
require 'fileutils'
require 'gitlab'
require 'json'
require 'optparse'
require 'tempfile'
require 'time'
require 'yaml'

require "bundler/gem_tasks"

require_relative 'lib/gdk'
require_relative 'lib/get'
require_relative 'lib/kubernetes'
require_relative 'lib/op'

Dir.glob('lib/tasks/**/*.rake').each { |r| import r }

task default: %w[tasks]

task :tasks do
  puts Rake::Task.tasks
end

if ENV.key?('TESTING')
  module FileUtils
    def sh(*cmd)
      # , &block)
      puts "I would run: #{cmd.join}"
    end

    def system(*cmd)
      # , &block)
      puts "I wanna run: #{cmd.join}"
    end
  end
end

CHART_DIR = '/Users/ibaum/src/charts/gitlab'.freeze
CLUSTER = 'ibaum-cluster'.freeze
PROJECT = 'group-geo-f9c951'.freeze
VALUES_FILE = "#{ENV.fetch('PWD')}/configs/mykube.yaml".freeze
begin
  URL = "https://gitlab.#{YAML.safe_load(File.read(VALUES_FILE))['global']['hosts']['domain']}".freeze
rescue NoMethodError
  warn "I don't see global.hosts.domain in the config"
  Kernel.exit 1
rescue Errno::ENOENT
  # Do nothing if the file doesn't exist
  nil
end
VAULT_UUID = '6ixjs3u5jkexv4b2qy6q2axzyq'.freeze
QA_RVM = 'gqas'.freeze
OMNIBUS_RVM = '2.6.6@omnibus'.freeze
API_URL = 'https://gitlab.com/api/v4'.freeze

RVM_RUBY_VERSION = File.read('.ruby-version').split('@').first

RELEASE_NAME = 'ibaum-gitlab'.freeze

# ENV['KUBECONFIG'] = "#{ENV['HOME']}/src/ibaum/rakeitall/demo/.kube/config"

# ENV['KUBECONFIG'] = "#{ENV['PWD']}/.kubeyconfigy"

def helm_command
  dir = ENV.fetch('CHART_DIR') || CHART_DIR
  cmd = "cd #{dir} && helm"
  cmd << ' --debug' if ENV['debug'].eql?('true')
  cmd
end

def root_password
  get_secret('ibaum-gitlab-gitlab-initial-root-password', '.data.password')
end

def grafana_password
  get_secret('gitlab-grafana-initial-password', '.data.password')
end

def postgresql_password
  get_secret('ibaum-gitlab-postgresql-password', '.data.postgresql-password')
end

def postgresql_superuser_password
  get_secret('ibaum-gitlab-postgresql-password', '.data.postgresql-postgres-password')
end

def get_secret(secret, path)
  `kubectl -n foo get secret #{secret} -o jsonpath={#{path}} | base64 --decode`
end

def ansible_playbook(hosts, playbook, vars={})
  if hosts.class == String && File.file?(hosts)
    hosts = File.read(hosts)
  else
    template = <<~HOSTS_TEMPLATE
      [all]
      <% hosts.each do |host| %>
      <%= host %>
      <% end %>
    HOSTS_TEMPLATE
    hosts = ERB.new(template, nil, '<>').result(binding)
  end

  Tempfile.open do |inventory|
    inventory.write hosts
    inventory.rewind
    cmd = "ansible-playbook -i #{inventory.path} #{playbook}"
    cmd << " -e @#{vars[:var_file]}" if vars.key?(:var_file)

    if vars.key?(:variables)
      vars[:variables].each_pair do |variable, value|
        cmd << " -e #{variable}=#{value}"
      end
    end
    puts `#{cmd}`
  end
end

def build_ansible(inventory, vault_file, branch = 'master')
  project = CGI.escape('gitlab-org/build/omnibus-gitlab-mirror')
  command = %(cd #{ANSIBLE_DIR} && git switch #{branch} && ansible-playbook -i #{inventory} -e @#{vault_file} --ask-vault-pass)
  command << " -l #{ENV['GROUPS']}" if ENV.key?('GROUPS')
  command << " -e 'package_url=https://gitlab.com/api/v4/projects/#{project}/jobs/#{ENV['JOB']}/artifacts/pkg/ubuntu-xenial/gitlab.deb'" if ENV.key?('JOB')
  command << ' -vvvv' if ENV.key?('DEBUG')
  command
end

def ansible_inventory(name)
  File.join(Dir.pwd, "ansible_inventories/#{name}.yml") if File.exist?("ansible_inventories/#{name}.yml")
end

def bye_now(message)
  warn message
  Kernel.exit 1
end

def job(job_raw)
  job_raw unless job_raw.start_with?('http')

  job_raw.split('/').last
end

# Waits for an active task-runner pod, then returns the name
def task_runner_pod
  pod_name = ''
  10.times do
    pod_name = `kubectl get pods --field-selector=status.phase=Running -l app=task-runner,release=ibaum-gitlab -o custom-columns=N:.metadata.name --no-headers`.chomp
    break unless pod_name.empty?

    puts 'Waiting for task-runner pod to appear'
    sleep 5
  end
  pod_name
end
