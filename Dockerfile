ARG ARCH=

FROM debian:stable-slim

RUN apt-get update && apt-get install --no-install-recommends -y bind9-dnsutils=1:9.18.19-1~deb12u1 && rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
